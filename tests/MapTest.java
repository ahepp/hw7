package hw7.tests;

import hw7.Map;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public abstract class MapTest {

    /**
     * Give access to unit to abstract OrderedMapTest class so we can run
     * more specific tests.
     */
    protected Map<Integer, String> unit;

    protected abstract Map<Integer, String> createUnit();

    @Before
    public void setupTests() {
        unit = this.createUnit();
    }

    @Test
    public void newSetEmpty() {
        assertEquals(0, unit.size());
    }

    @Test
    public void newSetEmptyIterator() {
        int c = 0;
        for (int s: unit) {
            c++;
        }
        assertEquals(0, c);
    }

    @Test
    public void insertNotEmpty() {
        unit.insert(0, "Magda");
        assertEquals(1, unit.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertAlreadyMappedTest() {
        unit.insert(0, "Magda");
        assertEquals(1, unit.size());
        unit.insert(1, "John");
        assertEquals(2, unit.size());
        unit.insert(0, "Tim");
        assertEquals(2, unit.size());
        unit.insert(1, "Jim");
        assertEquals(2, unit.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertNullKeyTest() {
        unit.insert(null, "Magda");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUndefKeyTest() {
        unit.get(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getNullKeyTest() {
        unit.get(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeNullKeyTest() {
        unit.remove(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeUndefKeyTest() {
        unit.remove(0);
    }



    @Test
    public void insertHas() {
        unit.insert(0, "Paul");
        assertTrue(unit.has(0));
        assertFalse(unit.has(1));
    }

    @Test
    public void insertRemoveHas() {
        unit.insert(0, "Paul");
        unit.insert(1, "Peter");

        assertTrue(unit.has(0));
        assertTrue(unit.has(1));
        assertFalse(unit.has(2));

        unit.remove(0);

        assertFalse(unit.has(0));
        assertTrue(unit.has(1));
        assertFalse(unit.has(2));
    }

    @Test
    public void putTest() {
        unit.insert(0, "Paul");
        unit.insert(1, "Mary");
        assertEquals("Paul", unit.get(0));

        assertFalse(unit.has(2));
        assertTrue(unit.has(0));
        assertTrue(unit.has(1));

        unit.put(0, "Pam");

        assertEquals("Pam", unit.get(0));
        assertTrue(unit.has(0));
        assertTrue(unit.has(1));

        unit.put(0, "Jim");

        assertEquals("Jim", unit.get(0));
        assertFalse(unit.has(2));
        assertTrue(unit.has(0));
        assertTrue(unit.has(1));
    }

    @Test
    public void insertManySizeConsistent() {
        for (int i = 0; i < 2000; i++) {
            unit.insert(i, String.valueOf(i * i));
        }
        assertEquals(2000, unit.size());
    }

    @Test
    public void iteratorWorks() {
        String[] data = {"Peter", "Paul", "Mary", "Beverly"};
        int[] keys = {0, 1, 2, 3};
        for (int k: keys) {
            unit.insert(k, data[k]);
        }
        for (int s: unit) {
            int count = 0;
            for (int k: keys) {
                if (s == k) {
                    count += 1;
                }
            }
            assertEquals(1, count);
        }
    }
}
