package hw7;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;
import java.util.EmptyStackException;
import java.util.Collections;
import java.util.Scanner;

/**
 * Class to create a search engine for files with a url and then keywords
 * below it.
 */
public final class JHUgle {

    private JHUgle() {}

    private static void handleSpec(String current,
                                   Stack<ArrayList<String>> input) {
        switch (current) {
            case "!":
                // Exit on "!"
                System.exit(0);
                break;
            case "?":
                if (input.empty()) {
                    System.out.print("");
                }
                else {
                    raListOutput(input.peek());
                }
                break;
            default:
                //nada
                break;
        }
    }

    //stack holds the keywords, which have values equal to the sites that
    //contain them.
    private static void handleOp(String current,
                                 Stack<ArrayList<String>> input) {
        ArrayList<String> a = null;
        ArrayList<String> b = null;
        try {
            a = input.pop();
            b = input.pop();
        }
        catch (EmptyStackException e) {
            //keep going
        }
        if (a == null || b == null) {
            return;
        }
        ArrayList<String> ray;
        switch (current) {
            //&& is intersection
            case "&&":
                ray = intersection(a, b);
                input.push(ray);
                break;
            //|| is union
            case "||":
                ray = union(a, b);
                input.push(ray);
                break;
            default:
                break;
        }
    }

    private static void raListOutput(ArrayList<String> ray) {
        int len = ray.size();
        for (int i = 0; i < len; i++) {
            System.out.println(ray.get(i));
        }
    }

    private static ArrayList<String> union(ArrayList<String> a,
                                    ArrayList<String> b) {
        Collections.sort(a);
        Collections.sort(b);
        String aStr;
        String bStr;
        ArrayList<String> union = new ArrayList<>();
        //iterate through both arraylists at once
        int aCounter = 0;
        int bCounter = 0;
        int aSize = a.size();
        int bSize = b.size();
        //1 2 4 10
        //2 4 7 9
        while (bCounter < bSize && aCounter < aSize) {
            aStr = a.get(aCounter);
            bStr = b.get((bCounter));
            //if aStr is larger than bStr
            //add bStr and increment bCounter
            if (aStr.compareTo(bStr) > 0) {
                union.add(bStr);
                bCounter++;
            }
            //aStr is smaller than bStr
            //add aStr, increment aCounter up one
            else if (aStr.compareTo(bStr) < 0) {
                union.add(aStr);
                aCounter++;
            }
            //if they are equal, add one and increment
            //both counters
            else {
                union.add(aStr);
                aCounter++;
                bCounter++;
            }
        }
        //reached the end of a first
        if (aCounter == aSize) {
            while (bCounter < bSize) {
                bStr = b.get(bCounter);
                union.add(bStr);
                bCounter++;
            }
        }
        //reached the end of b first
        else if (bCounter == bSize) {
            while (aCounter < aSize) {
                aStr = a.get(aCounter);
                union.add(aStr);
                aCounter++;
            }
        }
        return union;
    }

    private static ArrayList<String> intersection(ArrayList<String> a,
                                           ArrayList<String> b) {

        Collections.sort(a);
        Collections.sort(b);
        String aStr;
        String bStr;
        ArrayList<String> intersection = new ArrayList<>();
        //iterate through both arraylists at once
        int aCounter = 0;
        int bCounter = 0;
        int aSize = a.size();
        int bSize = b.size();
        //1 2 4 11 12
        //2 4 7 9 10 11
        while (bCounter < bSize && aCounter < aSize) {
            aStr = a.get(aCounter);
            bStr = b.get((bCounter));
            //if aStr is larger than bStr
            //increment bCounter
            if (aStr.compareTo(bStr) > 0) {
                bCounter++;
            }
            //aStr is smaller than bStr
            //increment aCounter
            else if (aStr.compareTo(bStr) < 0) {
                aCounter++;
            }
            //if they are equal, add one and increment
            //both counters
            else {
                intersection.add(aStr);
                aCounter++;
                bCounter++;
            }
        }
        return intersection;
    }

    private static ArrayList<String> fastSplit(String text) {
        ArrayList<String> result = new ArrayList<>(18);
        int start = 0;
        boolean prevCharIsSeparator = true;
        char[] chars = text.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if (c == '\t' || c == ' ') {
                if (!prevCharIsSeparator) {
                    result.add(text.substring(start, i));
                    prevCharIsSeparator = true;
                }
                start = i + 1;
            } else {
                prevCharIsSeparator = false;
            }
        }
        if (start < chars.length) {
            result.add(text.substring(start));
        }
        return result;
    }

    private static void createMap(Map<String, ArrayList<String>> hash,
                                  String file) throws IOException {
        BufferedReader inputStream = null;
        try {
            inputStream =  new BufferedReader(new FileReader(file));
        }
        catch (IOException e) {
            System.exit(-1);
        }
        String line;
        while ((line = inputStream.readLine()) != null) {
            String website = line;
            line = inputStream.readLine();
            ArrayList<String> keys = fastSplit(line);
            ArrayList<String> temp;
            for (String i: keys) {
                if (!hash.has(i)) {
                    temp = new ArrayList<>(10);
                    temp.add(website);
                    hash.insert(i, temp);
                }
                else {
                    temp = hash.get(i);
                    temp.add(website);
                    hash.put(i, temp);
                }
            }
        }
        System.out.println("Index Created");
    }

    /**
     * Main function to handle all of user input.
     * @param args file to be searched.
     * @throws IOException if file cannot be found.
     */
    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.err.println("Please supply a search file.");
            return;
        }
        Map<String, ArrayList<String>> hash = new HashMap<>();
        createMap(hash, args[0]);
        Stack<ArrayList<String>> input = new Stack<>();
        Scanner sc = new Scanner(System.in);
        String current;
        int len = 0;
        //this arrayList holds websites based on input words
        ArrayList<String> ray;
        // Keep reading while there is still input to be read
        while (sc.hasNext()) {
            // Take in input into a string
            current = sc.next();
            // See if input is ? or !
            if (current.matches("\\?") || current.matches("!")) {
                handleSpec(current, input);
            }
            // See if input has && or ||
            else if (current.length() == 2 &&
                    (current.contains("&&") || current.contains("||"))) {
                //can't call && or || on 1 or 0 elements
                if (len < 2) {
                    System.err.println("ERROR: not enough arguments");
                }
                else {
                    handleOp(current, input);
                    len--;
                }
            }
            else {
                //anything else gets pushed onto stack
                //and total arrayList gets updated with union
                if (hash.has(current)) {
                    ray = hash.get(current);
                    input.push(ray);
                    len++;
                }
                else {
                    System.err.println("Invalid key");
                }
            }
        }
    }
}