package hw7.tests;

import hw7.Map;
import hw7.HashMap;
import org.junit.Test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class HashMapTest extends MapTest {

    @Override
    protected Map<Integer, String> createUnit() {
        return new HashMap<>();
    }

    @Test
    public void insertTest() {
        unit.insert(0, "a");
        unit.insert(16, "b");
        assertTrue(unit.has(0));
        assertFalse(unit.has(1));
        unit.insert(1, "b");
        assertTrue(unit.has(1));
        assertEquals("a", unit.get(0));
        assertEquals("b", unit.get(16));
        assertEquals("b", unit.get(1));
        unit.put(0, "c");
        assertTrue(unit.has(0));
        assertEquals("c", unit.get(0));
    }

    @Test
    public void deleteTest() {
        assertFalse(unit.has(0));
        unit.insert(0, "a");
        unit.insert(1, "b");
        unit.insert(2, "c");
        assertEquals(3, unit.size());
        assertTrue(unit.has(2));
        unit.remove(0);
        assertTrue(unit.has(2));
        assertEquals(2, unit.size());
        assertFalse(unit.has(0));
        unit.insert(0, "d");
        System.out.println(unit);
    }
}
