package hw7;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Hash Map implementation using unoptimized linear probing.
 * @param <K> key
 * @param <V> value
 */
public class LinProbeHashMap<K, V> implements Map<K, V> {

    private class Entry {
        K key;
        V value;
        int hash;

        Entry() {
            this.hash = Integer.hashCode(0);
        }

        Entry(K k, V v) {
            this.key = k;
            this.value = v;
            this.hash = k.hashCode();
        }

        public String toString() {
            return "Entry<key: " + this.key
                    + "; value: " + this.value
                    + "; hash: " + this.hash
                    + ">";
        }

    }

    private Entry[] data;
    private int size;
    private int capacity;
    private float maxLoad;

    /**
     * Constructor to create an empty hash map.
     */
    public LinProbeHashMap() {
        this.size = 0;
        this.capacity = 16;
        this.data = (Entry[]) new LinProbeHashMap.Entry[this.capacity];
        this.maxLoad = 0.75f;
        for (int i = 0; i < this.capacity; i++) {
            this.data[i] = new Entry();
        }
    }

    @Override
    public void insert(K k, V v) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException("Cannot handle null key.");
        }
        if (this.has(k)) {
            throw new IllegalArgumentException("Cannot handle duplicate keys.");
        }
        Entry e = new Entry(k, v);
        if (((float) this.size / this.capacity) > this.maxLoad) {
            this.data = this.grow();
        }
        this.insert(this.data, e.hash & (this.capacity - 1), e);

    }

    private void insert(Entry[] en, int index, Entry e) {
        while (en[index].hash != Integer.hashCode(0)) {
            if (en[index].hash == Integer.hashCode(-1)) {
                break;
            }
            index = (index + 1 & (this.capacity - 1));
        }
        en[index] = e;
        this.size++;
    }

    private Entry[] grow() {
        this.capacity = this.capacity + this.capacity;
        Entry[] temp = (Entry[]) new LinProbeHashMap.Entry[this.capacity];
        for (int i = 0; i < this.capacity; i++) {
            temp[i] = new Entry();
        }
        this.size = 0;
        for (int i = 0; i < this.data.length; i++) {
            if (this.data[i].key == null) {
                continue;
            }
            insert(temp, data[i].hash & (this.capacity - 1),
                    this.data[i]);
        }
        return temp;
    }

    private int find(K k) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException("Cannot handle null key.");
        }
        int hash = k.hashCode() & (this.capacity - 1);
        while (this.data[hash].key != null && !this.data[hash].key.equals(k)) {
            hash = (hash + 1) & (this.capacity - 1);
        }
        if (this.data[hash].key == null) {
            return -1;
        }
        if (this.data[hash].hash == Integer.hashCode(-1)) {
            return -1;
        }
        return hash;
    }

    @Override
    public V remove(K k) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException("Cannot handle null key.");
        }
        int f = find(k);
        if (f == -1) {
            throw new IllegalArgumentException("Cannot find key.");
        }
        this.data[f].hash = -1;
        this.size--;
        return this.data[f].value;
    }

    @Override
    public void put(K k, V v) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException("Cannot handle null key.");
        }
        int f = this.find(k);
        if (f == -1) {
            throw new IllegalArgumentException("Key not found.");
        }
        this.data[f].value = v;
    }

    @Override
    public V get(K k) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException("Cannot handle null key.");
        }
        int f = this.find(k);
        if (!this.has(k)) {
            throw new IllegalArgumentException("Key not found.");
        }
        return this.data[f].value;
    }

    @Override
    public boolean has(K k) {
        return find(k) != -1;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public Iterator iterator() {
        ArrayList<K> list = new ArrayList<>();
        for (int i = 0; i < this.capacity; i++) {
            if (this.data[i].key != null) {
                list.add(this.data[i].key);
            }
        }
        return list.iterator();
    }


    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (Entry i: data) {
            if (i.key != null && i.hash != -1) {
                s.append(i.key);
                s.append(": ");
                s.append(i.value);
                s.append("\n");
            }
        }
        s.setLength(s.length() - 1);
        return s.toString();
    }
}