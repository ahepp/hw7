package hw7;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Hash Map implementation using separate chaining to deal with collisions.
 * @param <K> key.
 * @param <V> value.
 */
public class SepChainHashMap<K, V> implements Map<K, V> {

    private class Entry {
        K key;
        V value;
        Entry next;


        Entry(K k, V v) {
            this.value = v;
            this.key = k;
            //next is inherently null
        }
    }

    //Array where each element is a node
    private Entry[] data;
    private int size;
    private int capacity;
    private float maxLoad;

    /**
     * Constructor to create a default empty hash map.
     */
    public SepChainHashMap() {
        this.capacity = 16;
        this.size = 0;
        this.data = (Entry[]) new SepChainHashMap.Entry[this.capacity];
        this.maxLoad = 0.75f;

    }

    /**
     * Constructor that lets us set the initial capacity and load factor.
     * @param capacity intial capacity.
     * @param maxLoad maximum load factor before growing.
     */
    public SepChainHashMap(int capacity, float maxLoad) {
        if (capacity <= 0) {
            capacity = 16;
        }
        if (maxLoad <= 0) {
            maxLoad = 0.5f;
        }
        this.capacity = capacity;
        this.data = (Entry[]) new SepChainHashMap.Entry[this.capacity];
        this.maxLoad = maxLoad;
    }

    //get the array hashed index of key
    //duplicate keys hash to the same value
    private int getIndex(K key) {
        int hash = key.hashCode();
        return hash & (this.capacity - 1);
    }

    private Entry find(K k) {
        int index = getIndex(k);
        Entry head = this.data[index];
        while (head != null) {
            if (head.key.equals(k)) {
                return head;
            }
            head = head.next;
        }
        return null;
    }

    private Entry findForSure(K k) {
        Entry e = this.find(k);
        if (e == null) {
            throw new IllegalArgumentException("No such key");
        }
        return e;
    }

    @Override
    public void insert(K k, V v) {
        int index = getIndex(k);

        //index is empty, just insert
        if (this.data[index] == null) {
            Entry e = new Entry(k, v);
            this.data[index] = e;
            this.size++;
        }
        //something at that index -> we chain it at the end
        //need to search if key already exists though
        else {
            Entry head = this.data[index];
            //search through the list to find duplicate keys
            while (head.next != null) {
                if (head.key.equals(k)) {
                    throw new IllegalArgumentException("Duplicate Key Found");
                }
                head = head.next;
            }
            Entry e = new Entry(k, v);
            head.next = e;
            this.size++;
        }

        //check the load factor
        if (((float) this.size / this.capacity) > this.maxLoad) {
            //double the capacity of the array
            this.capacity *= 2;
            Entry[] temp = this.data;
            this.data = (Entry[]) new SepChainHashMap.Entry[this.capacity];
            this.size = 0;
            //go through the elements in data to rehash
            int len = temp.length;
            for (int i = 0; i < len; i++) {
                Entry h = temp[i];
                while (h != null) {
                    insert(h.key, h.value);
                    h = h.next;
                }
            }

        }
    }

    @Override
    public V remove(K k) {
        int index = getIndex(k);
        Entry head = this.data[index];
        //key isn't there
        if (head == null) {
            throw new IllegalArgumentException("No such key");
        }
        Entry prev = null;

        //brings us to the right position
        //1 -> 2 -> 3 -> null
        //looking for 2
        while (head.next != null) {
            if (head.key.equals(k)) {
                break;
            }
            prev = head;
            head = head.next;
        }

        //first node in the list
        if (prev == null) {
            V value = head.value;
            //only one element at that index list
            if (head.next == null) {
                this.data[index] = null;
            }
            //more than one element
            else {
                this.data[index] = head.next;
            }
            size--;
            return value;
        }

        //not the first element
        else {
            prev.next = head.next;
            size--;
            return head.value;
        }
    }

    @Override
    public void put(K k, V v) {
        Entry e = this.findForSure(k);
        e.value = v;
    }

    @Override
    public V get(K k) {
        Entry e = this.findForSure(k);
        return e.value;
    }

    @Override
    public boolean has(K k) {
        return this.find(k) != null;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public Iterator<K> iterator() {
        ArrayList<K> list = new ArrayList<>();
        for (int i = 0; i < this.capacity; i++) {
            Entry head = this.data[i];
            while (head != null) {
                list.add(head.key);
                head = head.next;
            }
        }
        return list.iterator();
    }


    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < this.capacity; i++) {
            if (this.data[i] != null) {
                Entry head = this.data[i];
                while (head != null) {
                    s.append(head.key);
                    s.append(": ");
                    s.append(head.value);
                    s.append("\n");
                    head = head.next;
                }
            }
        }

        return s.toString();
    }
}

